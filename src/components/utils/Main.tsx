import Link from "next/link";
import { Breadcrumb, IBreadcrumbItem } from "./Breadcrumb";

export function Main({
  children,
  breadcrumbs,
  title,
}: {
  children: React.ReactNode;
  breadcrumbs: Array<IBreadcrumbItem>;
  title?: React.ReactNode;
}) {
  return (
    <main className="flex flex-col flex-1">
      <nav className="bg-slate-500 text-white p-4 flex">
        <Breadcrumb className="w-full">{breadcrumbs}</Breadcrumb>
        {title}
      </nav>
      <div className="flex-1 p-4 overflow-auto">{children}</div>
    </main>
  );
}
