export default function Card({ children }: { children: React.ReactNode }) {
  return <div className="bg-slate-500 p-2 rounded">{children}</div>;
}
