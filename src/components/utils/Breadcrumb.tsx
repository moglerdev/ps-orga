import classNames from "classnames";
import Link from "next/link";
import { twMerge } from "tailwind-merge";

export interface IBreadcrumbItem {
  href?: string;
  label: string;
}

export function BreadcrumbLink({ children }: { children: IBreadcrumbItem }) {
  if (children.href === undefined)
    return <span className="text-slate-300">{children.label}</span>;
  return (
    <Link className="text-white" href={children.href}>
      {children.label}
    </Link>
  );
}

export function Breadcrumb({
  children,
  className,
}: {
  children: IBreadcrumbItem[];
  className?: string;
}) {
  return (
    <ol className={twMerge("flex", className)}>
      {children.map((item, i) => (
        <li>
          <BreadcrumbLink>{item}</BreadcrumbLink>
          {i < children.length - 1 && (
            <span className="text-gray-400 mx-2">/</span>
          )}
        </li>
      ))}
    </ol>
  );
}
