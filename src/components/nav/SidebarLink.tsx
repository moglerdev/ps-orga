"use client";

import classNames from "classnames";
import Link from "next/link";
import { usePathname } from "next/navigation";
import { twMerge } from "tailwind-merge";

export function SidebarLink({
  children,
  href,
  exact,
}: {
  children: React.ReactNode;
  href: string;
  exact?: boolean;
}) {
  const path = usePathname();
  const isActive = exact ? path === href : path.startsWith(href);

  const className = classNames("block text-slate-400 hover:text-white p-2", {
    "text-white": isActive,
  });

  return (
    <Link href={href} className={twMerge(className)}>
      {children}
    </Link>
  );
}
