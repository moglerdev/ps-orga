import Link from "next/link";
import {
  AiFillDashboard,
  AiFillEdit,
  AiFillSetting,
  AiOutlineUser,
} from "react-icons/ai";
import { SidebarLink } from "./SidebarLink";

export function Tooltip({
  children,
  title,
}: {
  title: string;
  children: React.ReactNode;
}) {
  return (
    <div className="group relative flex">
      {children}
      <span className="absolute left-20 top-3 scale-0 transition-all rounded bg-gray-800 p-2 text-lg text-white group-hover:scale-100">
        {title}
      </span>
    </div>
  );
}

export function Sidebar() {
  return (
    <aside className="h-screen bg-slate-600 text-5xl flex flex-col">
      <nav className="h-full">
        <Tooltip title="Dashboard">
          <SidebarLink href="/" exact>
            <AiFillDashboard />
          </SidebarLink>
        </Tooltip>
        <Tooltip title="Diary">
          <SidebarLink href="/diary">
            <AiFillEdit />
          </SidebarLink>
        </Tooltip>
      </nav>
      <nav>
        <Tooltip title="User">
          <SidebarLink href="/user">
            <AiOutlineUser />
          </SidebarLink>
        </Tooltip>
        <Tooltip title="Settings">
          <SidebarLink href="/settings">
            <AiFillSetting />
          </SidebarLink>
        </Tooltip>
      </nav>
    </aside>
  );
}
