import { Main } from "@/components/utils/Main";

export default function Home() {
  const breadcrumbs = [{ href: "/", label: "Home" }, { label: "Dashboard" }];
  return (
    <Main breadcrumbs={breadcrumbs}>
      <h2>Dashboard</h2>
    </Main>
  );
}
