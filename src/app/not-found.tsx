import { Main } from "@/components/utils/Main";
import Link from "next/link";

export default function NotFound() {
  const breadcrumbs = [{ href: "/", label: "Home" }, { label: "Not Found" }];
  return (
    <Main breadcrumbs={breadcrumbs}>
      <h2>Not Found</h2>
      <p>Could not find requested resource</p>
      <Link className="text-blue-400" href="/">
        Return Home
      </Link>
    </Main>
  );
}
