import Card from "@/components/utils/Card";
import { Main } from "@/components/utils/Main";
import Link from "next/link";
import { AiFillFileAdd } from "react-icons/ai";

function DiaryTitle() {
  return (
    <button className="text-xl">
      <AiFillFileAdd />
    </button>
  );
}

export default function Diary() {
  const breadcrumbs = [{ href: "/", label: "Home" }, { label: "Diary" }];
  const ranged = [];
  for (let i = 1; i <= 3100; i++) {
    ranged.push(i);
  }
  return (
    <Main breadcrumbs={breadcrumbs} title={<DiaryTitle />}>
      <div className="gap-2 grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3">
        {ranged.map((day) => (
          <Link href={`/diary/${day}`}>
            <Card>
              <h1 className="text-xl">{day}</h1>
            </Card>
          </Link>
        ))}
      </div>
    </Main>
  );
}
